package com.example.traineecheck.Admin;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.traineecheck.Data.AdminModel;
import com.example.traineecheck.Data.TeacherModel;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.example.traineecheck.Data.StudentModel;
import com.example.traineecheck.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import org.checkerframework.checker.nullness.qual.NonNull;

public class AccountList extends AppCompatActivity {

    private FirebaseFirestore db;
    private FirestoreRecyclerAdapter<StudentModel, StudentViewHolder> studentAdapter;
    private FirestoreRecyclerAdapter<AdminModel, AdminViewHolder> adminAdapter;
    private FirestoreRecyclerAdapter<TeacherModel, TeacherViewHolder> teacherAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_list);

        db = FirebaseFirestore.getInstance();

        // Configure the RecyclerView for Students
        RecyclerView studentRecyclerView = findViewById(R.id.studentList);
        studentRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        Query studentQuery = db.collection("Students").orderBy("Name", Query.Direction.ASCENDING);
        FirestoreRecyclerOptions<StudentModel> studentOptions = new FirestoreRecyclerOptions.Builder<StudentModel>()
                .setQuery(studentQuery, StudentModel.class)
                .build();
        studentAdapter = new FirestoreRecyclerAdapter<StudentModel, StudentViewHolder>(studentOptions) {
            @Override
            protected void onBindViewHolder(@NonNull StudentViewHolder holder, int position, @NonNull StudentModel model) {
                holder.emailTextView.setText(model.getEmail());
                holder.NameTextView.setText(model.getName());
            }

            @NonNull
            @Override
            public StudentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.acc_view_student, parent, false);
                return new StudentViewHolder(view);
            }
        };
        studentRecyclerView.setAdapter(studentAdapter);

        // Configure the RecyclerView for Admin
        RecyclerView adminRecyclerView = findViewById(R.id.adminList);
        adminRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        Query adminQuery = db.collection("Admin").orderBy("Name", Query.Direction.ASCENDING);
        FirestoreRecyclerOptions<AdminModel> adminOptions = new FirestoreRecyclerOptions.Builder<AdminModel>()
                .setQuery(adminQuery, AdminModel.class)
                .build();
        adminAdapter = new FirestoreRecyclerAdapter<AdminModel, AdminViewHolder>(adminOptions) {
            @Override
            protected void onBindViewHolder(@NonNull AdminViewHolder holder, int position, @NonNull AdminModel model) {
                holder.adminFieldTextView.setText(model.getEmail());
                holder.adminNameTextView.setText(model.getName());
            }

            @NonNull
            @Override
            public AdminViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.acc_view_admin, parent, false);
                return new AdminViewHolder(view);
            }
        };
        adminRecyclerView.setAdapter(adminAdapter);

        // Configure the RecyclerView for Teachers
        RecyclerView teacherRecyclerView = findViewById(R.id.teacherList);
        teacherRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        Query teacherQuery = db.collection("Teachers").orderBy("Name", Query.Direction.ASCENDING);
        FirestoreRecyclerOptions<TeacherModel> teacherOptions = new FirestoreRecyclerOptions.Builder<TeacherModel>()
                .setQuery(teacherQuery, TeacherModel.class)
                .build();
        teacherAdapter = new FirestoreRecyclerAdapter<TeacherModel, TeacherViewHolder>(teacherOptions) {
            @Override
            protected void onBindViewHolder(@NonNull TeacherViewHolder holder, int position, @NonNull TeacherModel model) {
                holder.teacherFieldTextView.setText(model.getEmail());
                holder.teacherNameTextView.setText(model.getName());
            }

            @NonNull
            @Override
            public TeacherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.acc_view_teacher, parent, false);
                return new TeacherViewHolder(view);
            }
        };
        teacherRecyclerView.setAdapter(teacherAdapter);
    }

    public class StudentViewHolder extends RecyclerView.ViewHolder {
        TextView emailTextView;
        TextView NameTextView;

        ImageButton editButton;
        ImageButton deleteButton;

        public StudentViewHolder(@NonNull View itemView) {
            super(itemView);

            NameTextView = itemView.findViewById(R.id.name_list);

            emailTextView = itemView.findViewById(R.id.email_list);
            editButton = itemView.findViewById(R.id.edit_Button);

            emailTextView = itemView.findViewById(R.id.email_list); // Update with the correct ID
            deleteButton = itemView.findViewById(R.id.delete_Button); // Update with the correct ID

            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        showRoleEditDialog("Student", position);
                    }
                }
            });

            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        // Build an alert dialog to confirm the deletion
                        new AlertDialog.Builder(itemView.getContext(), R.style.CustomAlertDialogStyle)
                                .setTitle("Confirm Deletion")
                                .setMessage("Are you sure you want to delete this student?")
                                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // User confirmed the deletion
                                        DocumentReference studentRef = studentAdapter.getSnapshots().getSnapshot(position).getReference();
                                        studentRef.delete()
                                                .addOnSuccessListener(aVoid -> Toast.makeText(itemView.getContext(), "Student deleted", Toast.LENGTH_SHORT).show())
                                                .addOnFailureListener(e -> Toast.makeText(itemView.getContext(), "Failed to delete student", Toast.LENGTH_SHORT).show());
                                    }
                                })
                                .setNegativeButton("Cancel", null)
                                .show();
                    }
                }
            });
        }
    }

    public class AdminViewHolder extends RecyclerView.ViewHolder {
        TextView adminFieldTextView;
        TextView adminNameTextView; // Add this TextView for displaying the admin's name
        ImageButton editButton;
        ImageButton deleteButton;

        public AdminViewHolder(@NonNull View itemView) {
            super(itemView);

            adminFieldTextView = itemView.findViewById(R.id.a_email_list);
            adminNameTextView = itemView.findViewById(R.id.a_name_list); // Add this line
            editButton = itemView.findViewById(R.id.a_edit_Button);
            deleteButton = itemView.findViewById(R.id.a_delete_Button);

            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        // Check if the current user can edit this admin
                        String adminUidToEdit = adminAdapter.getSnapshots().getSnapshot(position).getId();
                        String currentAdminUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                        if (!adminUidToEdit.equals(currentAdminUid)) {
                            showRoleEditDialog("Admin", position);
                        } else {
                            // Display a Toast message if the current user is trying to edit themselves
                            Toast.makeText(itemView.getContext(), "You cannot edit yourself.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        // Get the UID of the admin to be deleted
                        String adminUidToDelete = adminAdapter.getSnapshots().getSnapshot(position).getId();

                        // Get the UID of the current admin user
                        String currentAdminUid = FirebaseAuth.getInstance().getCurrentUser().getUid();

                        // Check if the UID of the admin to be deleted matches the UID of the current admin
                        if (adminUidToDelete.equals(currentAdminUid)) {
                            // The current admin is trying to delete itself, show a message
                            Toast.makeText(AccountList.this, "You cannot delete yourself.", Toast.LENGTH_SHORT).show();
                        } else {
                            // Build an alert dialog to confirm the deletion
                            new AlertDialog.Builder(AccountList.this, R.style.CustomAlertDialogStyle)
                                    .setTitle("Confirm Deletion")
                                    .setMessage("Are you sure you want to delete this admin?")
                                    .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // User confirmed the deletion
                                            DocumentReference adminRef = adminAdapter.getSnapshots().getSnapshot(position).getReference();

                                            // Check the number of admin items
                                            adminRef.getParent().get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                    if (task.isSuccessful()) {
                                                        QuerySnapshot querySnapshot = task.getResult();
                                                        if (querySnapshot != null && querySnapshot.size() > 1) {
                                                            // There are more than one admin items, proceed with deletion
                                                            adminRef.delete()
                                                                    .addOnSuccessListener(aVoid -> Toast.makeText(AccountList.this, "Admin deleted", Toast.LENGTH_SHORT).show())
                                                                    .addOnFailureListener(e -> Toast.makeText(AccountList.this, "Failed to delete admin", Toast.LENGTH_SHORT).show());
                                                        } else {
                                                            // Only one admin item left, show a message that it can't be deleted
                                                            Toast.makeText(AccountList.this, "Cannot delete the last admin.", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                    })
                                    .setNegativeButton("Cancel", null)
                                    .show();
                        }
                    }
                }
            });

        }
    }


    public class TeacherViewHolder extends RecyclerView.ViewHolder {
        TextView teacherFieldTextView;
        TextView teacherNameTextView;

        ImageButton editButton;
        ImageButton deleteButton;

        public TeacherViewHolder(@NonNull View itemView) {
            super(itemView);

            teacherFieldTextView = itemView.findViewById(R.id.t_email_list);
            editButton = itemView.findViewById(R.id.t_edit_Button);

            teacherNameTextView = itemView.findViewById(R.id.t_name_list);

            deleteButton = itemView.findViewById(R.id.t_delete_Button);

            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        showRoleEditDialog("Teacher", position);
                    }
                }
            });

            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        // Build an alert dialog to confirm the deletion
                        new AlertDialog.Builder(itemView.getContext(), R.style.CustomAlertDialogStyle)
                                .setTitle("Confirm Deletion")
                                .setMessage("Are you sure you want to delete this teacher?")
                                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // User confirmed the deletion
                                        DocumentReference teacherRef = teacherAdapter.getSnapshots().getSnapshot(position).getReference();
                                        teacherRef.delete()
                                                .addOnSuccessListener(aVoid -> Toast.makeText(itemView.getContext(), "Teacher deleted", Toast.LENGTH_SHORT).show())
                                                .addOnFailureListener(e -> Toast.makeText(itemView.getContext(), "Failed to delete teacher", Toast.LENGTH_SHORT).show());
                                    }
                                })
                                .setNegativeButton("Cancel", null)
                                .show();
                    }
                }
            });

        }
    }

    private void showRoleEditDialog(String userType, int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.CustomAlertDialogStyle);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.acc_list_disable, null);

        // Get references to dialog elements
        EditText newRoleEditText = dialogView.findViewById(R.id.edit_role);

        builder.setView(dialogView)
                .setTitle("Disable Account")
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newRole = newRoleEditText.getText().toString().trim();
                        if (!newRole.isEmpty()) {
                            // Update the role in Firestore
                            updateRoleInFirestore(userType, position, newRole);
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void updateRoleInFirestore(String userType, int position, String newRole) {
        DocumentReference userRef;
        switch (userType) {
            case "Student":
                userRef = studentAdapter.getSnapshots().getSnapshot(position).getReference();
                break;
            case "Admin":
                userRef = adminAdapter.getSnapshots().getSnapshot(position).getReference();
                break;
            case "Teacher":
                userRef = teacherAdapter.getSnapshots().getSnapshot(position).getReference();
                break;
            default:
                // Handle invalid userType
                return;
        }

        // Update the user's role in Firestore
        userRef.update("Role", newRole)
                .addOnSuccessListener(aVoid -> {
                    Toast.makeText(this, "Role updated successfully", Toast.LENGTH_SHORT).show();
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(this, "Failed to update role", Toast.LENGTH_SHORT).show();
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        studentAdapter.startListening();
        adminAdapter.startListening();
        teacherAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        studentAdapter.stopListening();
        adminAdapter.stopListening();
        teacherAdapter.stopListening();
    }
}

