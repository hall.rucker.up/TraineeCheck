package com.example.traineecheck.Admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.traineecheck.LogIn.StudLog;
import com.example.traineecheck.R;
import com.example.traineecheck.Student.MainStudent;
import com.example.traineecheck.Teacher.MainTeacher;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class AdminPage extends AppCompatActivity {

    private EditText emailEditText, passwordEditText;
    private ImageButton togglePasswordButton;
    private boolean isPasswordVisible = false;

    boolean empty = false;
    FirebaseAuth f_auth;
    FirebaseFirestore f_store;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_page);

        f_store = FirebaseFirestore.getInstance();
        f_auth = FirebaseAuth.getInstance();

        emailEditText = findViewById(R.id.emailEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        togglePasswordButton = findViewById(R.id.togglePasswordButton);

        togglePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                togglePasswordVisibility();
            }
        });

        findViewById(R.id.Sign1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkField(emailEditText);
                checkField(passwordEditText);

                if (empty) {
                    f_auth.signInWithEmailAndPassword(emailEditText.getText().toString(), passwordEditText.getText().toString()).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                        @Override
                        public void onSuccess(AuthResult authResult) {
                            Toast.makeText(AdminPage.this, "Checking", Toast.LENGTH_SHORT).show();
                            checkRole(authResult.getUser().getUid());
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(AdminPage.this, "Invalid Request", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    private void togglePasswordVisibility() {
        if (isPasswordVisible) {
            passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            togglePasswordButton.setImageResource(R.drawable.unchecked);
        } else {
            passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            togglePasswordButton.setImageResource(R.drawable.checked);
        }

        isPasswordVisible = !isPasswordVisible;
        passwordEditText.setSelection(passwordEditText.getText().length());
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private boolean checkField(EditText edittext) {
        String text = edittext.getText().toString().trim(); // Remove leading/trailing white spaces

        if (text.isEmpty()) {
            edittext.setError("This field is required.");
            empty = false;
        } else if (edittext == emailEditText && !text.contains("@")) {
            Toast.makeText(AdminPage.this, "Please include '@' in your email address", Toast.LENGTH_SHORT).show();
            emailEditText.setError("Invalid email address.");
            empty = false;
        } else {
            empty = true;
        }
        return empty;
    }

    private void checkRole(String uid) {
        DocumentReference df = f_store.collection("Admin").document(uid);
        df.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Log.d("TAG", "onSuccess" + documentSnapshot.getData());

                String role = documentSnapshot.getString("Role");
                if ("Admin".equals(role)) {
                    Toast.makeText(AdminPage.this, "Welcome", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), MainAdmin.class));
                    finish();
                } else if ("Teacher".equals(role)) {
                    Toast.makeText(AdminPage.this, "Invalid Request", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), MainTeacher.class));
                    finish();
                }
                else if ("Student".equals(role)) {
                    Toast.makeText(AdminPage.this, "Invalid Request", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), MainStudent.class));
                    finish();
                }
            }
        });

    }
    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if (currentUser != null) {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            DocumentReference userRef = db.collection("Admin").document(currentUser.getUid());

            userRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    if (documentSnapshot.exists()) {
                        String userRole = documentSnapshot.getString("Role");

                        if ("Admin".equals(userRole)) {
                            startActivity(new Intent(getApplicationContext(), MainAdmin.class));
                            finish();
                        }
                        // Add conditions for other roles, if needed
                        else if ("Student".equals(userRole)) {
                            Toast.makeText(AdminPage.this, "This is a Student account", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), MainStudent.class));
                            finish();
                            // Add logic for teacher role
                        } else if ("Teacher".equals(userRole)) {
                            Toast.makeText(AdminPage.this, "This is a Teacher account", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), MainTeacher.class));
                            finish();
                            // Add logic for admin role
                        } else {
                            Toast.makeText(AdminPage.this, "Invalid request", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(AdminPage.this, "...", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}