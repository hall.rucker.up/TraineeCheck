package com.example.traineecheck.Admin;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.traineecheck.MainActivity;
import com.example.traineecheck.R;
import com.example.traineecheck.Student.MainStudent;
import com.google.firebase.auth.FirebaseAuth;

public class ChooseCreate extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_create);

        findViewById(R.id.Create1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChooseCreate.this, CreateAccountAdmin.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.Create2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChooseCreate.this, CreateAccountTeach.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.Create3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChooseCreate.this, CreateAccountStud.class);
                startActivity(intent);
            }
        });

    }
}