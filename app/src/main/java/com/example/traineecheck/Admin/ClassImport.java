package com.example.traineecheck.Admin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.traineecheck.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClassImport extends AppCompatActivity {

    private AutoCompleteTextView autoCompleteTextView;
    private Button addStudentButton;
    private ListView classListView;
    private Button saveButton;
    private AutoCompleteTextView teacherSelector;

    private FirebaseFirestore db;
    private DocumentReference selectedTeacherDocument;

    private List<String> studentList = new ArrayList<>();
    private List<String> selectedStudents = new ArrayList<>();
    private List<String> teacherList = new ArrayList<>();
    private ArrayAdapter<String> studentAdapter;
    private ArrayAdapter<String> teacherAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_import);

        autoCompleteTextView = findViewById(R.id.autoCompleteTextView);
        addStudentButton = findViewById(R.id.addStudentButton);
        classListView = findViewById(R.id.classListView);
        saveButton = findViewById(R.id.saveButton);
        teacherSelector = findViewById(R.id.teacherSelector);

        db = FirebaseFirestore.getInstance();

        studentAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, studentList);
        teacherAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, teacherList);

        autoCompleteTextView.setAdapter(studentAdapter);
        teacherSelector.setAdapter(teacherAdapter);

        addStudentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String student = autoCompleteTextView.getText().toString().trim();
                if (!student.isEmpty() && !selectedStudents.contains(student)) {
                    selectedStudents.add(student);
                    updateClassListView();
                    autoCompleteTextView.setText("");
                } else if (selectedStudents.contains(student)) {
                    Toast.makeText(ClassImport.this, "Student already in the list", Toast.LENGTH_SHORT).show();
                }
            }
        });

        teacherSelector.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedTeacherName = teacherSelector.getText().toString();
                getTeacherDocument(selectedTeacherName, new TeacherDocumentCallback() {
                    @Override
                    public void onTeacherDocumentRetrieved(DocumentReference teacherDocument) {
                        selectedTeacherDocument = teacherDocument;
                    }
                });
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveClassList();
            }
        });

        fetchStudentList();
        fetchTeacherList();
    }

    private void getTeacherDocument(final String teacherName, final TeacherDocumentCallback callback) {
        db.collection("Teachers")
                .whereEqualTo("Name", teacherName)
                .limit(1)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (!queryDocumentSnapshots.isEmpty()) {
                            DocumentSnapshot documentSnapshot = queryDocumentSnapshots.getDocuments().get(0);
                            DocumentReference teacherDocument = documentSnapshot.getReference();
                            callback.onTeacherDocumentRetrieved(teacherDocument);
                        } else {
                            callback.onTeacherDocumentRetrieved(null);
                        }
                    }
                });
    }

    private void fetchStudentList() {
        db.collection("Students")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        studentList.clear();
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            String studentName = document.getString("Name");
                            studentList.add(studentName);
                        }
                        studentAdapter.notifyDataSetChanged();
                    }
                });
    }

    private void updateClassListView() {
        ArrayAdapter<String> selectedStudentsAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                selectedStudents
        );
        classListView.setAdapter(selectedStudentsAdapter);
    }

    private void fetchTeacherList() {
        db.collection("Teachers")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        teacherList.clear();
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            String teacherName = document.getString("Name");
                            teacherList.add(teacherName);
                        }
                        teacherAdapter.notifyDataSetChanged();
                    }
                });
    }

    private void saveClassList() {
        if (selectedTeacherDocument != null) {
            Map<String, Object> classListData = new HashMap<>();
            classListData.put("classList", selectedStudents);

            selectedTeacherDocument.update(classListData)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(ClassImport.this, "Class list saved successfully", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            Toast.makeText(ClassImport.this, "Please select a teacher.", Toast.LENGTH_SHORT).show();
        }
    }
}

interface TeacherDocumentCallback {
    void onTeacherDocumentRetrieved(DocumentReference teacherDocument);
}
