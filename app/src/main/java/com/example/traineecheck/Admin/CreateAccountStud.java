package com.example.traineecheck.Admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.traineecheck.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class CreateAccountStud extends AppCompatActivity {

    EditText full_name, _email, _pass, _contact;
    Button _create;
    boolean empty = false;

    private FirebaseAuth f_auth;
    private FirebaseFirestore f_store;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_account);

        full_name = findViewById(R.id.fullNameEditText);
        _email = findViewById(R.id.emailEditText);
        _pass = findViewById(R.id.passwordEditText);
        _contact = findViewById(R.id.contactEditText);
        _create = findViewById(R.id.CreateBtn);

        f_auth = FirebaseAuth.getInstance();
        f_store = FirebaseFirestore.getInstance();

        _create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkField(full_name);
                checkField(_email);
                checkField(_pass);
                checkField(_contact);

                if (empty) {
                    f_auth.createUserWithEmailAndPassword(
                            _email.getText().toString(), _pass.getText().toString()).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                        @Override
                        public void onSuccess(AuthResult authResult) {
                            FirebaseUser user = f_auth.getCurrentUser();
                            String studentId = f_auth.getUid();
                            Toast toast =  Toast.makeText(CreateAccountStud.this, "Account Created", Toast.LENGTH_SHORT);
                            View toastView = toast.getView();
                            toastView.setBackgroundColor(Color.GREEN); // Set background color to red
                            TextView toastMessage = toastView.findViewById(android.R.id.message);
                            toastMessage.setTextColor(Color.WHITE); // Set text color to white
                            toastMessage.setGravity(Gravity.CENTER);
                            toast.show();
                            DocumentReference df = f_store.collection("Students").document(user.getUid());
                            Map<String, Object> user_info = new HashMap<>();
                            user_info.put("studentId", studentId);
                            user_info.put("Name", full_name.getText().toString());
                            user_info.put("Email", _email.getText().toString());
                            user_info.put("Pass", _pass.getText().toString());
                            user_info.put("Contact", _contact.getText().toString());
                            user_info.put("Role", "Student");
                            df.set(user_info);

                            // Clear the EditText fields
                            full_name.setText("");
                            _email.setText("");
                            _pass.setText("");
                            _contact.setText("");

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(CreateAccountStud.this, "Email already exist", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    private boolean checkField(EditText edittext) {
        String text = edittext.getText().toString().trim(); // Remove leading/trailing white spaces

        if (text.isEmpty()) {
            edittext.setError("This field is required.");
            empty = false;
        } else if (edittext == _email && !text.contains("@")) {
            edittext.setError("Invalid email address.");
            empty = false;
        } else if (edittext == _pass && text.length() < 8) {
            edittext.setError("Password must be at least 8 characters.");
            empty = false;
        } else {
            empty = true;
        }
        return empty;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}