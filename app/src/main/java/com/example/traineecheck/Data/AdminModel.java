package com.example.traineecheck.Data;

public class AdminModel {
    private String Email;
    private String Name;

    public AdminModel() {}

    public AdminModel(String email, String name) {
        Email = email;
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public String getName() {
        return Name;
    }
}