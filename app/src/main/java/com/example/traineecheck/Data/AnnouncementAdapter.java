package com.example.traineecheck.Data;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.example.traineecheck.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.auth.User;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AnnouncementAdapter extends RecyclerView.Adapter<AnnouncementAdapter.ViewHolder> {

    private List<AnnouncementModel> announcements = new ArrayList<>();
    private FirebaseFirestore db;
    private FirebaseUser currentUser;
    private Context context;
    private String userRole;
    private OnItemLongClickListener longClickListener;

    public AnnouncementAdapter(Context context, OnItemLongClickListener longClickListener) {
        this.context = context;
        db = FirebaseFirestore.getInstance();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        this.longClickListener = longClickListener;

        fetchUserRole();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_announcement_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AnnouncementModel announcement = announcements.get(position);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        String timestampString = dateFormat.format(announcement.getTimestamp());
        holder.userNameTextView.setText(announcement.getUserName());
        holder.titleTextView.setText(announcement.getTitle());
        holder.contentTextView.setText(announcement.getContent());
        holder.timestampTextView.setText(timestampString);

        holder.itemView.setOnLongClickListener(view -> {
            if (longClickListener != null) {
                // Check if the user's Role is "Student"
                if ("Student".equals(userRole)) { // Use the userRole field
                    // Handle the long-press action here
                } else {
                    longClickListener.onItemLongClick(position, announcement);
                    assignClickListener(holder, announcement); // Assign click listener
                }
            }
            return true;
        });
    }


    @Override
    public int getItemCount() {
        return announcements.size();
    }

    public void setAnnouncements(List<AnnouncementModel> announcements) {
        this.announcements = announcements;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView userNameTextView;
        TextView titleTextView;
        TextView contentTextView;
        TextView timestampTextView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            userNameTextView = itemView.findViewById(R.id.userNameTextView);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            contentTextView = itemView.findViewById(R.id.contentTextView);
            timestampTextView = itemView.findViewById(R.id.announcementTimestamp);
        }
    }

    public interface OnItemLongClickListener {
        void onItemLongClick(int position, AnnouncementModel announcement);
    }

    // Method to delete an announcement
    private void deleteAnnouncement(AnnouncementModel announcement) {
        String documentId = announcement.getDocumentId();
        db.collection("Announcements")
                .document(documentId)
                .delete()
                .addOnSuccessListener(aVoid -> {
                    // The announcement was successfully deleted
                    Toast.makeText(context, "Announcement deleted", Toast.LENGTH_SHORT).show();
                })
                .addOnFailureListener(e -> {
                    // Handle errors, e.g., display an error message
                    Toast.makeText(context, "Error deleting announcement: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                });
    }

    private void assignClickListener(ViewHolder holder, AnnouncementModel announcement) {
        // Add click listener to delete an announcement
        holder.itemView.setOnClickListener(view -> {
            if (currentUser != null && announcement.getPosterId() != null) {
                if (announcement.getPosterId().equals(currentUser.getUid())) {
                    // The current user is the poster, allow deletion
                    deleteAnnouncement(announcement);
                } else {
                    // The current user is not the poster, display an error message or handle it as needed
                    Toast.makeText(context, "You don't have permission to delete this announcement", Toast.LENGTH_SHORT).show();
                }
            } else {
                // Handle the case where currentUser or posterId is null
                Toast.makeText(context, "You don't have permission to delete this announcement", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fetchUserRole() {
        if (currentUser != null) {
            String userId = currentUser.getUid();
            DocumentReference studentRef = db.collection("Students").document(userId);

            studentRef.get().addOnSuccessListener(documentSnapshot -> {
                if (documentSnapshot.exists()) {
                    userRole = documentSnapshot.getString("Role");
                    notifyDataSetChanged(); // Notify the adapter to update based on user's role
                }
            });
        }
    }

}

