package com.example.traineecheck.Data;

import java.util.Date;


public class AnnouncementModel {
    private String userName;
    private String title;
    private String content;
    private Date timestamp; // Add timestamp field
    private String posterId;
    private String announcementId;
    private String documentId;
    private String Role;

    public AnnouncementModel() {
        // Default constructor required for Firestore
    }

    public AnnouncementModel(String userName, String title, String content, Date timestamp) {
        this.userName = userName;
        this.title = title;
        this.content = content;
        this.timestamp = timestamp;
    }

    // Getters and setters for all fields
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getPosterId() {
        return posterId;
    }

    public void setPosterId(String posterId) {
        this.posterId = posterId;
    }

    public String getAnnouncementId() {
        return announcementId;
    }

    public void setAnnouncementId(String announcementId) {
        this.announcementId = announcementId;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String role) {
        Role = role;
    }
}


