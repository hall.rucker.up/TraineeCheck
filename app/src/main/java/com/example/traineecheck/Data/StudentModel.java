package com.example.traineecheck.Data;

public class StudentModel {
    private String Email;
    private String Name;

    public StudentModel() {}

    public StudentModel(String email, String name) {
        Email = email;
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public String getName() {
        return Name;
    }
}