package com.example.traineecheck.Data;

public class TeacherModel {
    private String Email;
    private String Name;
    private String Role;
    private String Contact;


    public TeacherModel() {}

    public TeacherModel(String email, String name) {
        Email = email;
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public String getName() {
        return Name;
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String role) {
        Role = role;
    }

    public String getContact() {
        return Contact;
    }

    public void setContact(String contact) {
        Contact = contact;
    }
}