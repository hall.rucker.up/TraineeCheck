package com.example.traineecheck.Data;

public class Timestamp {
    private String amTimeIn;
    private String amTimeOut;
    private String pmTimeIn;
    private String pmTimeOut;

    public Timestamp() {
        // Default constructor required for DataSnapshot.getValue(Timestamp.class)
    }

    public String getAmTimeIn() {
        return amTimeIn;
    }

    public String getAmTimeOut() {
        return amTimeOut;
    }

    public String getPmTimeIn() {
        return pmTimeIn;
    }

    public String getPmTimeOut() {
        return pmTimeOut;
    }
}

