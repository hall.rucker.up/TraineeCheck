package com.example.traineecheck.Data;
import com.example.traineecheck.R;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Map;

public class TimestampAdapter extends RecyclerView.Adapter<TimestampAdapter.ViewHolder> {

    private List<String> studentIds;
    private Map<String, Timestamp> studentTimestamps;

    public TimestampAdapter(List<String> studentIds, Map<String, Timestamp> studentTimestamps) {
        this.studentIds = studentIds;
        this.studentTimestamps = studentTimestamps;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.timestamp_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String studentId = studentIds.get(position);
        Timestamp timestamp = studentTimestamps.get(studentId);

        if (timestamp != null) {
            String displayText = "Student ID: " + studentId + "\nAM Time In: " + timestamp.getAmTimeIn()
                    + "\nAM Time Out: " + timestamp.getAmTimeOut() + "\nPM Time In: " + timestamp.getPmTimeIn()
                    + "\nPM Time Out: " + timestamp.getPmTimeOut();
            holder.timestampText.setText(displayText);
        } else {
            holder.timestampText.setText("Student ID: " + studentId + "\nNo Timestamps Available");
        }
    }

    @Override
    public int getItemCount() {
        return studentIds.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView timestampText;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            timestampText = itemView.findViewById(R.id.timestampText);
        }
    }
}
