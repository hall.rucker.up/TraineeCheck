package com.example.traineecheck.Data;

public class pdfClass {
    private String name;
    private String url;

    public pdfClass() {
        // Default constructor required for Firestore
    }

    public pdfClass(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }
}