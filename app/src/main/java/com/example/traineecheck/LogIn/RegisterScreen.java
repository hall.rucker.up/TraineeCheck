package com.example.traineecheck.LogIn;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.traineecheck.R;

public class RegisterScreen extends AppCompatActivity {

    private EditText emailEditText, fullNameEditText, roleEditText, contactNoEditText;
    private Button sendButton;
    boolean empty = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_screen);

        emailEditText = findViewById(R.id.emailEditText);
        fullNameEditText = findViewById(R.id.fullNameEditText);
        roleEditText = findViewById(R.id.roleEditText);
        contactNoEditText = findViewById(R.id.contactNoEditText);
        sendButton = findViewById(R.id.SendBtn);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkField(fullNameEditText);
                checkField(emailEditText);
                checkField(roleEditText) ;
                checkField(contactNoEditText);
                sendEmail();
            }
        });
    }

    private boolean checkField(EditText edittext) {
        if(edittext.getText().toString().isEmpty()){
            edittext.setError("This field is required.");
            empty = false;
        }
        else {
            empty = true;
        }
        return empty;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private void sendEmail() {

        String senderEmail = emailEditText.getText().toString();
        String fullName = fullNameEditText.getText().toString();
        String role = roleEditText.getText().toString();
        String contactNo = contactNoEditText.getText().toString();

        if (senderEmail.isEmpty() || fullName.isEmpty() || role.isEmpty() || contactNo.isEmpty()) {

            return;
        }

        String emailContent = "Full Name: " + fullName + "\nRole: " + role + "\nContact No: " + contactNo;

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("plain/text");

        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{senderEmail});
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"traineecheck@gmail.com"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Registration Details");
        emailIntent.putExtra(Intent.EXTRA_TEXT, emailContent);

        startActivity(Intent.createChooser(emailIntent, "Send Email"));
    }

}