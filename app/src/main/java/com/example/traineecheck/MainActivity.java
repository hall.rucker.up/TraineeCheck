package com.example.traineecheck;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.traineecheck.Admin.AdminPage;
import com.example.traineecheck.LogIn.StudLog;
import com.example.traineecheck.LogIn.TeachLog;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.adminBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AdminPage.class);
                startActivity(intent);
            }
        });

    }
    public void Role1(View view) {
        startActivity(new Intent(MainActivity.this, StudLog.class));
    }

    public void Role2(View view) {
        startActivity(new Intent(MainActivity.this, TeachLog.class));
    }
}