package com.example.traineecheck;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.traineecheck.Admin.MainAdmin;
import com.example.traineecheck.Student.MainStudent;
import com.example.traineecheck.Teacher.MainTeacher;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        if (savedInstanceState == null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    navigateToNextActivity();
                }
            }, 3000);
//            startActivity(new Intent(SplashScreen.this, MainAdmin.class));
//            finish();
//        }
//    },3000);// Delay for 3 seconds
        }
    }

    boolean userFound = false; // Flag to indicate if the user was found
    boolean startActivityCalled = false; // Flag to indicate if the startActivity() method was called

    protected void navigateToNextActivity() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if (currentUser != null) {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            String uid = currentUser.getUid();

            String[] collectionNames = { "Students", "Teachers", "Admin" };

            for (String collectionName : collectionNames) {
                if (userFound) {
                    break;
                }

                DocumentReference userRef = db.collection(collectionName).document(uid);

                userRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            String userRole = documentSnapshot.getString("Role");

                            if ("Teacher".equals(userRole)) {
                                // Add logic for teacher role
                                userFound = true;
                                if (!startActivityCalled) {
                                    String teachName = documentSnapshot.getString("Name");
                                    String welcomeMessage = "Welcome, " + teachName;
                                    Toast.makeText(SplashScreen.this, welcomeMessage, Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(SplashScreen.this, MainTeacher.class));
                                    finish();
                                    startActivityCalled = true;
                                }
                            } else if ("Student".equals(userRole)) {
                                    // Add logic for student role
                                userFound = true;
                                if (!startActivityCalled) {
                                    String studentName = documentSnapshot.getString("Name");
                                    String welcomeMessage = "Welcome, " + studentName;
                                    Toast.makeText(SplashScreen.this, welcomeMessage, Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(SplashScreen.this, MainStudent.class));
                                    finish();
                                    startActivityCalled = true;
                                }
                            } else if ("Admin".equals(userRole)) {
                                // Add logic for admin role
                                userFound = true;
                                if (!startActivityCalled) {
                                    String adminName = documentSnapshot.getString("Name");
                                    String welcomeMessage = "Welcome, " + adminName;
                                    startActivity(new Intent(SplashScreen.this, MainAdmin.class));
                                    finish();
                                    startActivityCalled = true;
                                }

                            }
                            else if ("Role".equals(null)) {
                                userFound = true;
                                if (!startActivityCalled) {
                                    startActivity(new Intent(SplashScreen.this, MainActivity.class));
                                    finish();
                                    startActivityCalled = true;
                                }

                            }
                            else if ("Role".equals("null")) {
                                userFound = true;
                                if (!startActivityCalled) {
                                    Toast toast = Toast.makeText(SplashScreen.this, "Your account has been disabled", Toast.LENGTH_SHORT);                         View toastView = toast.getView();
                                    toastView.setBackgroundColor(Color.RED); // Set background color to red
                                    TextView toastMessage = toastView.findViewById(android.R.id.message);
                                    toastMessage.setTextColor(Color.WHITE); // Set text color to white
                                    toastMessage.setGravity(Gravity.CENTER);
                                    toast.show();
                                    startActivity(new Intent(SplashScreen.this, MainActivity.class));
                                    finish();
                                    startActivityCalled = true;
                                }

                            }
                            else {
                                userFound = true;
                                if (!startActivityCalled) {
                                    String studentName = documentSnapshot.getString("Name");
                                    String welcomeMessage = "Welcome, " + studentName;
                                    Toast.makeText(SplashScreen.this, welcomeMessage, Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(SplashScreen.this, MainActivity.class));
                                    finish();
                                    startActivityCalled = true;
                                }
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(SplashScreen.this, "Failed", Toast.LENGTH_SHORT).show();
                        userFound = false;
                        if (!startActivityCalled) {
                            startActivity(new Intent(SplashScreen.this, MainActivity.class));
                            finish();
                            startActivityCalled = true;
                        }
                    }
                });
            }
        } else {
            userFound = false;
            if (!startActivityCalled) {
                startActivity(new Intent(SplashScreen.this, MainActivity.class));
                finish();
                startActivityCalled = true;
            } // Optional: Finish the SplashScreen activity
        }
    }
}