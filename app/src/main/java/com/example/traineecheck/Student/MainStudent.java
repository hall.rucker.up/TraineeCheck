package com.example.traineecheck.Student;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.traineecheck.MainActivity;
import com.example.traineecheck.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.Objects;

public class MainStudent extends AppCompatActivity {

    FirebaseAuth f_auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_student);

        f_auth = FirebaseAuth.getInstance();

        findViewById(R.id.dash_1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainStudent.this, StudentAttendance.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.dash_2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainStudent.this, TeacherList.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.dash_3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainStudent.this, ViewAnnouncement.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.dash_4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainStudent.this, UploadPDF.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.dash_5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainStudent.this, StudProfile.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.SignOutStud1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogoutStud();
            }
        });

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        String userId = Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid();

        DocumentReference userRef = db.collection("Students").document(userId);

        userRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    String userName = documentSnapshot.getString("Name");

                    TextView nameTextView = findViewById(R.id.name_user);
                    nameTextView.setText(userName);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(MainStudent.this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void LogoutStud() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.CustomAlertDialogStyle);
        builder.setTitle("Log out");
        builder.setMessage("Are you sure you want to logout?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                f_auth.signOut();
                startActivity(new Intent(MainStudent.this, MainActivity.class));
                finish();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
//    public void dash_1(View view) {
//        startActivity(new Intent(MainStudent.this, StudProfile.class));
//        finish();
//    }
//    public void dash_2(View view) {
//        startActivity(new Intent(MainStudent.this, StudProfile.class));
//        finish();
//    }
//    public void dash_3(View view) {
//        startActivity(new Intent(MainStudent.this, StudProfile.class));
//        finish();
//    }
//    public void dash_4(View view) {
//        startActivity(new Intent(MainStudent.this, StudProfile.class));
//        finish();
//    }
//    public void dash_5(View view) {
//        startActivity(new Intent(MainStudent.this, StudProfile.class));
//        finish();
//    }
}