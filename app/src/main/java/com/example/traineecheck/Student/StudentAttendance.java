package com.example.traineecheck.Student;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.traineecheck.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class StudentAttendance extends AppCompatActivity {

    private Button amInButton, amOutButton, pmInButton, pmOutButton, overInButton, overOutButton;
    private FirebaseAuth auth;
    private DatabaseReference dailyAttendanceRef;

    private boolean isCooldownActive = false;
    private Timer cooldownTimer = new Timer();

    private TextView amInTimestampTextView, amOutTimestampTextView, pmInTimestampTextView, pmOutTimestampTextView, overInTimestampTextView, overOutTimestampTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_attendance);

        amInButton = findViewById(R.id.amInButton);
        amOutButton = findViewById(R.id.amOutButton);
        pmInButton = findViewById(R.id.pmInButton);
        pmOutButton = findViewById(R.id.pmOutButton);
        overInButton = findViewById(R.id.overInButton);
        overOutButton = findViewById(R.id.overOutButton);

        auth = FirebaseAuth.getInstance();
        String userId = auth.getCurrentUser().getUid();

        // Firebase Realtime Database reference
        dailyAttendanceRef = FirebaseDatabase.getInstance().getReference("Students").child(userId).child("DailyAttendance");

        // Find the TextView elements
        amInTimestampTextView = findViewById(R.id.amInTimestampTextView);
        amOutTimestampTextView = findViewById(R.id.amOutTimestampTextView);
        pmInTimestampTextView = findViewById(R.id.pmInTimestampTextView);
        pmOutTimestampTextView = findViewById(R.id.pmOutTimestampTextView);
        overInTimestampTextView = findViewById(R.id.overInTimestampTextView);
        overOutTimestampTextView = findViewById(R.id.overOutTimestampTextView);

        // Initialize the TextViews with timestamps
        addTimestampListener(amInTimestampTextView, "AM Time In");
        addTimestampListener(amOutTimestampTextView, "AM Time Out");
        addTimestampListener(pmInTimestampTextView, "PM Time In");
        addTimestampListener(pmOutTimestampTextView, "PM Time Out");
        addTimestampListener(overInTimestampTextView, "overTime In");
        addTimestampListener(overOutTimestampTextView, "overTime Out");

        amInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCheckInConfirmationDialog("AM Time In");
            }
        });

        amOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCheckInConfirmationDialog("AM Time Out");
            }
        });

        pmInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCheckInConfirmationDialog("PM Time In");
            }
        });

        pmOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCheckInConfirmationDialog("PM Time Out");
            }
        });

        overInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCheckInConfirmationDialog("overTime In");
            }
        });

        overOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCheckInConfirmationDialog("overTime Out");
            }
        });
    }

    private void showCheckInConfirmationDialog(final String fieldName) {
        if (isCooldownActive) {
            // Show a message indicating that the user needs to wait before clicking again
            Toast.makeText(StudentAttendance.this, "Please wait for 60 seconds before clicking again.", Toast.LENGTH_SHORT).show();
            return;
        }

        // Disable the button during the cooldown
        // You can disable the button if needed.

        // Set a cooldown period (e.g., 10 seconds)
        isCooldownActive = true;
        cooldownTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                // After the cooldown period, re-enable the button
                isCooldownActive = false;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Re-enable the button if you previously disabled it
                    }
                });
            }
        }, 60000); // 60,000 milliseconds (60 seconds) cooldown
        // You can adjust the cooldown time as needed.

        // Continue with the check-in confirmation dialog as previously mentioned
        dailyAttendanceRef.child(getCurrentDate()).child(fieldName).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                    // A timestamp for the current field already exists, show a toast message
                    Toast.makeText(StudentAttendance.this, fieldName + " has already been clicked today", Toast.LENGTH_SHORT).show();
                } else {
                    // No timestamp for the current field, confirm check-in
                    AlertDialog.Builder builder = new AlertDialog.Builder(StudentAttendance.this, R.style.CustomAlertDialogStyle);
                    builder.setTitle("Confirm Check-In");
                    builder.setMessage("Are you sure you want to check in for " + fieldName + "?");

                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Save the server timestamp to the database
                            dailyAttendanceRef.child(getCurrentDate()).child(fieldName).setValue(ServerValue.TIMESTAMP);
                            dialog.dismiss();
                        }
                    });

                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Handle any errors here
                Toast.makeText(StudentAttendance.this, "Error occurred. Please try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Method to get the current date in the format "yyyy-MM-dd"
    private String getCurrentDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return sdf.format(new Date());
    }

    private String formatElapsedTime(long timestamp) {
        if (timestamp > 0) {
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.getDefault());
            String formattedTime = sdf.format(new Date(timestamp));
            return formattedTime;
        }
        return null; // If timestamp is invalid, return null
    }

    private void addTimestampListener(final TextView textView, final String fieldName) {
        ValueEventListener timestampListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.child(fieldName).getValue() != null) {
                    long timestamp = dataSnapshot.child(fieldName).getValue(Long.class);
                    String formattedTime = formatElapsedTime(timestamp);
                    if (formattedTime != null) {
                        textView.setText("Time: " + formattedTime +" ✓");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Handle any errors here
            }
        };

        dailyAttendanceRef.child(getCurrentDate()).addValueEventListener(timestampListener);
    }
}
