package com.example.traineecheck.Student;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.accounts.Account;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.traineecheck.Data.TeacherModel;
import com.example.traineecheck.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;


import org.checkerframework.checker.nullness.qual.NonNull;

public class TeacherList extends AppCompatActivity {

    private TextView teacherNameTextView;
    private TextView teacherEmailTextView;
    private TextView teacherRoleTextView;
    private TextView teacherContactTextView;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_list);

        teacherNameTextView = findViewById(R.id.th_name_list);
        teacherEmailTextView = findViewById(R.id.th_email_list);
        teacherRoleTextView = findViewById(R.id.th_role_list);
        teacherContactTextView = findViewById(R.id.th_contact_list);
        db = FirebaseFirestore.getInstance();

        // Get the current user's ID (assuming the user is a student)
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            String currentUserId = currentUser.getUid();

            // Query the "Students" collection to get the student's assigned teacherId
            DocumentReference studentRef = db.collection("Students").document(currentUserId);
            studentRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            String teacherId = document.getString("teacherId");
                            if (teacherId != null) {
                                // Fetch teacher's information based on the student's assigned teacherId
                                fetchTeacherInformation(teacherId);
                            } else {
                                // Student doesn't have an assigned teacher
                                showToast("You do not have an assigned teacher.");
                            }
                        }
                    } else {
                        // Handle the error
                        showToast("Error fetching student information.");
                    }
                }
            });
        }
    }

    private void fetchTeacherInformation(String teacherId) {
        // Query the "Teachers" collection to get the teacher's information
        DocumentReference teacherRef = db.collection("Teachers").document(teacherId);
        teacherRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        String teacherName = document.getString("Name");
                        String teacherEmail = document.getString("Email");
                        String teacherRole = document.getString("Role");
                        String teacherContact = document.getString("Contact");
                        // Update the UI to display the teacher's information
                        teacherNameTextView.setText(teacherName);
                        teacherEmailTextView.setText(teacherEmail);
                        teacherRoleTextView.setText(teacherRole);
                        teacherContactTextView.setText(teacherContact);
                    } else {
                        // Handle the case when the teacher document doesn't exist
                        showToast("Teacher information not found.");
                    }
                } else {
                    // Handle the error
                    showToast("Error fetching teacher information.");
                }
            }
        });
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
