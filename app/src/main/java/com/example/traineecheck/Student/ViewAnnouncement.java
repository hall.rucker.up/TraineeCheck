package com.example.traineecheck.Student;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.traineecheck.Data.AnnouncementAdapter;
import com.example.traineecheck.Data.AnnouncementModel;
import com.example.traineecheck.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.List;

public class ViewAnnouncement extends AppCompatActivity {

    private FirebaseFirestore db;
    private RecyclerView announcementRecyclerView;
    private AnnouncementAdapter announcementAdapter;
    private FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stud_view_announcement);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        db = FirebaseFirestore.getInstance();
        announcementRecyclerView = findViewById(R.id.announcementRecyclerView);

        // Set up RecyclerView to display announcements
        announcementAdapter = new AnnouncementAdapter(this, new AnnouncementAdapter.OnItemLongClickListener() {
            @Override
            public void onItemLongClick(int position, AnnouncementModel announcement) {
                // Handle long press here, e.g., show a delete confirmation dialog
                showDeleteConfirmationDialog(position, announcement);
            }
        });
        announcementRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        announcementRecyclerView.setAdapter(announcementAdapter);

        // Load and display announcements from Firestore
        loadAnnouncements();
    }

    private void loadAnnouncements() {
        db.collection("Announcements")
                .orderBy("timestamp", Query.Direction.DESCENDING)
                .addSnapshotListener((value, error) -> {
                    if (error != null) {
                        Log.e("ViewAnnouncement", "Error getting announcements: " + error.getMessage());
                        return;
                    }

                    if (value != null) {
                        List<AnnouncementModel> announcements = new ArrayList<>();
                        for (DocumentSnapshot document : value.getDocuments()) {
                            AnnouncementModel announcement = document.toObject(AnnouncementModel.class);
                            if (announcement != null) {
                                announcement.setDocumentId(document.getId()); // Set the document ID
                                announcements.add(announcement);
                            }
                        }
                        announcementAdapter.setAnnouncements(announcements);
                    }
                });
    }

    private void showDeleteConfirmationDialog(int position, AnnouncementModel announcement) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.CustomAlertDialogStyle);
        builder.setTitle("Delete Announcement");
        builder.setMessage("Are you sure you want to delete this announcement?");
        builder.setPositiveButton("Delete", (dialog, which) -> {
            deleteAnnouncement(position, announcement);
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> {
            dialog.dismiss();
        });
        builder.show();
    }

    private void deleteAnnouncement(int position, AnnouncementModel announcement) {
        // Check if the current user is authenticated and if the poster ID is not null
        if (currentUser != null && announcement.getPosterId() != null) {
            if (announcement.getPosterId().equals(currentUser.getUid())) {
                // The current user is the poster, allow deletion
                String documentId = announcement.getDocumentId();
                db.collection("Announcements")
                        .document(documentId)
                        .delete()
                        .addOnSuccessListener(aVoid -> {
                            // The announcement was successfully deleted
                            Toast.makeText(this, "Announcement deleted", Toast.LENGTH_SHORT).show();
                        })
                        .addOnFailureListener(e -> {
                            // Handle errors, e.g., display an error message
                            Toast.makeText(this, "Error deleting announcement: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        });
            } else {
                // The current user is not the poster, display an error message or handle it as needed
                Toast.makeText(this, "You don't have permission to delete this announcement", Toast.LENGTH_SHORT).show();
            }
        } else {
            // Handle the case where currentUser or posterId is null
            Toast.makeText(this, "You don't have permission to delete this announcement", Toast.LENGTH_SHORT).show();
        }
    }
}
