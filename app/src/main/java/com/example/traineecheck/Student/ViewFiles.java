package com.example.traineecheck.Student;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.traineecheck.Data.pdfClass;
import com.example.traineecheck.Data.FileAdapter;
import com.example.traineecheck.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.ArrayList;
import java.util.List;

public class ViewFiles extends AppCompatActivity {

    FloatingActionButton floatingActionButton;
    ListView listView;
    DatabaseReference databaseReference;
    List<pdfClass> uploads;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stud_view_files);

            floatingActionButton=findViewById(R.id.float_btn);
            listView=findViewById(R.id.listview);
            uploads=new ArrayList<>();


            //create method

            viewAllFiles();

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void  onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    pdfClass pdfupload = uploads.get(i);

                    Intent intent=new Intent(Intent.ACTION_VIEW);
                    intent.setType("application/pdf");
                    intent.setData(Uri.parse(pdfupload.getUrl()));
                    startActivity(intent);
                }
            });

            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(getApplicationContext(),UploadPDF.class);
                    startActivity(intent);
                }
            });

        }

        private void viewAllFiles() {

            databaseReference= FirebaseDatabase.getInstance().getReference("Uploads");
            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    for (DataSnapshot postsnapshot:snapshot.getChildren()){

                        pdfClass pdfClass=postsnapshot.getValue(com.example.traineecheck.Data.pdfClass.class);

                        uploads.add(pdfClass);

                    }
                    String[] Uploads=new String[uploads.size()];
                    for (int i=0;i<Uploads.length;i++){
                        Uploads[i]=uploads.get(i).getName();
                    }
                    ArrayAdapter<String> adapter=new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_list_item_1,Uploads){

                        @NonNull
                        @Override
                        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View view = super.getView(position, convertView, parent);
                            TextView text=(TextView) view.findViewById(android.R.id.text1);
                            text.setTextColor(Color.BLACK);
                            text.setTextSize(22);
                            return view;
                        }
                    };

                    listView.setAdapter(adapter);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

        }
    }
