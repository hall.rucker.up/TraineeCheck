package com.example.traineecheck.Teacher;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.traineecheck.Data.AnnouncementModel;
import com.example.traineecheck.LogIn.RegisterScreen;
import com.example.traineecheck.LogIn.StudLog;
import com.example.traineecheck.R;
import com.example.traineecheck.Student.ViewAnnouncement;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.traineecheck.Data.AnnouncementAdapter;
import com.example.traineecheck.Data.AnnouncementModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import java.util.Date;

public class AnnouncementActivity extends AppCompatActivity {

    private FirebaseAuth auth;
    private FirebaseFirestore db;
    private EditText titleEditText, contentEditText;
    private Button postButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teach_announcement);

        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        titleEditText = findViewById(R.id.titleEditText);
        contentEditText = findViewById(R.id.contentEditText);
        postButton = findViewById(R.id.postButton);

        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAnnouncement();
            }
        });
    }

    private void createAnnouncement() {
        String title = titleEditText.getText().toString().trim();
        String content = contentEditText.getText().toString().trim();
        FirebaseUser currentUser = auth.getCurrentUser();

        if (currentUser != null && !title.isEmpty() && !content.isEmpty()) {
            String userId = currentUser.getUid();
            Date timestamp = new Date(); // Get the current timestamp

            // Perform a Firestore query to check if the user is a teacher
            db.collection("Teachers")
                    .document(userId)
                    .get()
                    .addOnSuccessListener(documentSnapshot -> {
                        if (documentSnapshot.exists()) {
                            String userName = documentSnapshot.getString("Name");

                            AnnouncementModel announcement = new AnnouncementModel(userName, title, content, timestamp);
                            announcement.setPosterId(userId); // Set the poster ID

                            db.collection("Announcements")
                                    .add(announcement)
                                    .addOnSuccessListener(documentReference -> {
                                        // Announcement added successfully
                                        titleEditText.setText("");
                                        contentEditText.setText("");
                                        showSuccessToast("Announcement posted successfully!"); // Show a success toast
                                    })
                                    .addOnFailureListener(e -> {
                                        // Handle the error
                                    });
                        } else {
                            // Handle the case where the user is not found in the Teachers collection
                            showFailureToast("You are not authorized to post announcements"); // Show an error toast
                        }
                    })
                    .addOnFailureListener(e -> {
                        // Handle the Firestore query error
                    });
        }
    }


    private void showSuccessToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void showFailureToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void ViewPost(View view) {
        startActivity(new Intent(AnnouncementActivity.this, ViewAnnouncement.class));
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}

