package com.example.traineecheck.Teacher;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.traineecheck.Data.StudentModel;
import com.example.traineecheck.Data.TeacherModel;
import com.example.traineecheck.R;
import com.example.traineecheck.Student.TeacherList;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import org.checkerframework.checker.nullness.qual.NonNull;

public class StudentList extends AppCompatActivity {

    private RecyclerView recyclerView;
    private FirebaseFirestore db;
    private FirestoreRecyclerAdapter<TeacherModel, AccountViewHolder> accountAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        db = FirebaseFirestore.getInstance();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if (currentUser != null) {
            String currentUserId = currentUser.getUid();

            // Query to retrieve students assigned to the current teacher
            CollectionReference studentsRef = db.collection("Students");
            Query query = studentsRef.whereEqualTo("teacherId", currentUserId)
                    .orderBy("Name", Query.Direction.ASCENDING);;

            FirestoreRecyclerOptions<TeacherModel> options = new FirestoreRecyclerOptions.Builder<TeacherModel>()
                    .setQuery(query, TeacherModel.class)
                    .build();

            accountAdapter = new FirestoreRecyclerAdapter<TeacherModel, AccountViewHolder>(options) {
                @NonNull
                @Override
                public AccountViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.acc_view_all, parent, false);
                    return new AccountViewHolder(view);
                }

                @Override
                protected void onBindViewHolder(@NonNull AccountViewHolder holder, int position, @NonNull TeacherModel model) {
                    holder.emailTextView.setText(model.getEmail());
                    holder.nameTextView.setText(model.getName());
                }
            };

            recyclerView.setAdapter(accountAdapter);
        } else {
            // Handle the case when the current user is not authenticated
            // You can show an error message or redirect the user to the login screen
            Log.e("TAG", "User is not authenticated.");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (accountAdapter != null) {
            accountAdapter.startListening();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (accountAdapter != null) {
            accountAdapter.stopListening();
        }
    }

    public static class AccountViewHolder extends RecyclerView.ViewHolder {
        TextView emailTextView;
        TextView nameTextView;

        public AccountViewHolder(@NonNull View itemView) {
            super(itemView);
            emailTextView = itemView.findViewById(R.id.z_email_list);
            nameTextView = itemView.findViewById(R.id.z_name_list);
        }
    }
}
