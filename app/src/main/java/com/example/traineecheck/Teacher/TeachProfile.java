package com.example.traineecheck.Teacher;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.traineecheck.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Objects;

public class TeachProfile extends AppCompatActivity {

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private TextView nameTextView;
    private TextView contactTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teach_profile);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        String userId = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();

        DocumentReference userRef = db.collection("Teachers").document(userId);

        userRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    String userName = documentSnapshot.getString("Name");
                    String userEmail = documentSnapshot.getString("Email");
                    String userRole = documentSnapshot.getString("Role");
                    String userContact = documentSnapshot.getString("Contact");

                    nameTextView = findViewById(R.id.teachName);
                    contactTextView = findViewById(R.id.teachContact);

                    if (userName != null) {
                        nameTextView.setText(userName);
                    }

                    if (userEmail != null) {
                        TextView emailTextView = findViewById(R.id.teachEmail);
                        emailTextView.setText(userEmail);
                    }

                    if (userRole != null) {
                        TextView roleTextView = findViewById(R.id.teachRole);
                        roleTextView.setText(userRole);
                    }

                    if (userContact != null) {
                        contactTextView.setText(userContact);
                    }
                }
            }
        });

        ImageButton editNameButton = findViewById(R.id.t_name_edit_Button);
        ImageButton editContactButton = findViewById(R.id.t_contact_edit_Button);

        // Set click listeners for edit buttons
        editNameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEditNameDialog();
            }
        });

        editContactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEditContactDialog();
            }
        });
    }

    private void showEditNameDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.CustomAlertDialogStyle);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.acc_name_edit, null);
        builder.setView(dialogView);

        EditText editNameEditText = dialogView.findViewById(R.id.editNameEditText);

        editNameEditText.setText(nameTextView.getText().toString());

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String newName = editNameEditText.getText().toString();
                if (!newName.isEmpty()) {
                    nameTextView.setText(newName);

                    // Update the name in the database
                    updateNameInDatabase(newName);

                    dialog.dismiss();
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showEditContactDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.CustomAlertDialogStyle);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.acc_contact_edit, null);
        builder.setView(dialogView);

        EditText editContactEditText = dialogView.findViewById(R.id.editContactEditText);


        editContactEditText.setText(contactTextView.getText().toString());

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String newContact = editContactEditText.getText().toString();
                if (!newContact.isEmpty()) {
                    contactTextView.setText(newContact);

                    // Update the contact in the database
                    updateContactInDatabase(newContact);

                    dialog.dismiss();
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void updateNameInDatabase(String newName) {
        String userId = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();
        DocumentReference userRef = db.collection("Teachers").document(userId);

        userRef
                .update("Name", newName)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Name updated successfully in the database
                    }
                })
                .addOnFailureListener(e -> {
                    // Handle the error
                });
    }

    private void updateContactInDatabase(String newContact) {
        String userId = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();
        DocumentReference userRef = db.collection("Teachers").document(userId);

        userRef
                .update("Contact", newContact)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Contact updated successfully in the database
                    }
                })
                .addOnFailureListener(e -> {
                    // Handle the error
                });
    }
}