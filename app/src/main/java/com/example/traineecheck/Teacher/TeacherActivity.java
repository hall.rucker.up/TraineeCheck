package com.example.traineecheck.Teacher;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.example.traineecheck.R;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TeacherActivity extends AppCompatActivity {

    private EditText dateInput;
    private Button searchButton;
    private TextView resultText;

    private ImageButton selectDateButton;

    private FirebaseFirestore db;
    private DatabaseReference realtimeDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher);

        selectDateButton = findViewById(R.id.selectDateButton);
        dateInput = findViewById(R.id.dateInput);
        searchButton = findViewById(R.id.searchButton);
        resultText = findViewById(R.id.resultText);

        db = FirebaseFirestore.getInstance();
        FirebaseApp.initializeApp(this);
        realtimeDb = FirebaseDatabase.getInstance().getReference();


        // Set an onClickListener for the date picker button
        selectDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog();
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchStudentAttendance();
            }
        });
    }

    private void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                // Handle the selected date
                String selectedDate = year + "-" + (month + 1) + "-" + dayOfMonth; // Adjust month (0-11 to 1-12)
                dateInput.setText(selectedDate);
            }
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    private void searchStudentAttendance() {
        String selectedDate = dateInput.getText().toString().trim();

        if (selectedDate.isEmpty()) {
            // Show an error message or return early if the date input is empty.
            Toast.makeText(this, "Please enter a valid date.", Toast.LENGTH_SHORT).show();
            return;
        }

        // Assuming the current user is a teacher, get the current teacher's user ID
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            String currentTeacherId = currentUser.getUid();

            // Query the Firestore "Students" collection for students with the same teacherId
            db.collection("Students")
                    .whereEqualTo("teacherId", currentTeacherId) // Filter students by teacherId
                    .orderBy("Name", Query.Direction.ASCENDING)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                resultText.setText(""); // Clear the previous result

                                for (QueryDocumentSnapshot studentDocument : task.getResult()) {
                                    String studentName = studentDocument.getString("Name");
                                    String studentId = studentDocument.getId(); // Capture student ID

                                    // Query Realtime Database for student attendance using the captured student ID
                                    realtimeDb.child("Students").child(studentId).child("DailyAttendance").child(selectedDate)
                                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    if (dataSnapshot.exists()) {
                                                        resultText.append("Student Name: " + studentName + "\n");

                                                        for (DataSnapshot timestampSnapshot : dataSnapshot.getChildren()) {
                                                            String fieldName = timestampSnapshot.getKey();
                                                            Object timestampValue = timestampSnapshot.getValue();
                                                            String formattedTime = formatTimestamp(timestampValue);
                                                            resultText.append(fieldName + ": " + formattedTime + "\n");
                                                        }
                                                        resultText.append("\n");
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                    // Handle any errors here
                                                }
                                            });
                                }
                            }
                        }
                    });
        }
    }


    private String formatTimestamp(Object timestampValue) {
        if (timestampValue instanceof Long) {
            long timestamp = (Long) timestampValue;
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.getDefault());
            return sdf.format(new Date(timestamp));
        } else {
            return "N/A";
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}